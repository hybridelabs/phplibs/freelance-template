module.exports = {
  purge: [],
  theme: {
    container: {
      center : true,
      padding: {
        default: '1rem',
        sm: '0',
      },
    },
    extend: {},
  },
  variants: {},
  plugins: [],
}
